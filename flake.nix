{
  description = "Aivero contrib";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/23.11";

    crane = {
      url = "github:ipetkov/crane";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, crane, flake-utils, ... }:
    let systems = [ "x86_64-linux" "aarch64-linux" ];
    in
    flake-utils.lib.eachSystem systems (system:
      let
        craneLib = (crane.mkLib nixpkgs.legacyPackages.${system});
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ ];
        };

        gst-depth-meta = pkgs.stdenv.mkDerivation ({
          name = "gst-depth-meta";
          src = ./gst-depth-meta;

          outputs = [
            "out"
            "dev"
          ];

          nativeBuildInputs = [
            pkgs.ninja
            pkgs.meson
            pkgs.pkg-config
          ];

          buildInputs = [
            pkgs.gst_all_1.gst-plugins-base
          ];
        });

        gst-colorizer = pkgs.stdenv.mkDerivation ({
          name = "gst-colorizer";
          src = ./gst-colorizer;

          outputs = [
            "out"
            "dev"
          ];

          nativeBuildInputs = [
            pkgs.ninja
            pkgs.cmake
            pkgs.pkg-config
          ];

          buildInputs = [
            pkgs.gst_all_1.gst-plugins-base
          ];
        });

        src = pkgs.lib.cleanSourceWith {
          src = ./.;
          filter = path: type:
            (pkgs.lib.hasSuffix "\.capnp" path) ||
            (pkgs.lib.hasSuffix "\.h" path) ||
            (craneLib.filterCargoSources path type)
          ;
        };

        gstBuildInputs = [
          pkgs.gst_all_1.gst-plugins-base
        ];

        contribCommon = {
          inherit src;

          pname = "contrib";
          version = "0.0.0";

          nativeBuildInputs = [
            pkgs.clang
            pkgs.capnproto
            pkgs.pkg-config

            pkgs.rustPlatform.bindgenHook
          ];

          buildInputs = [
            gst-depth-meta
            pkgs.librealsense
          ] ++ gstBuildInputs;
        };

        # Build *just* the cargo dependencies, so we can reuse
        # all of that work when running in CI
        cargoArtifacts = craneLib.buildDepsOnly contribCommon;

        contrib = craneLib.buildPackage (contribCommon // {
          inherit cargoArtifacts;

          postInstall = ''
            mkdir -p "$out/lib/gstreamer-1.0"
            find "$out/lib" -name '*.so' -type f |
               xargs '-d\n' -n1 basename |
               sed 's#^#../#' |
               xargs '-d\n' -I{} ln -s {} "$out/lib/gstreamer-1.0"
          '';

          # Tests are run with cargo-nextest
          # Disable here so we don't run the tests twice
          doCheck = false;
        });

        contribEnv = {
          GST_PLUGIN_SYSTEM_PATH_1_0 =
            pkgs.lib.makeSearchPathOutput "lib" "lib/gstreamer-1.0" (builtins.concatLists [
              gstBuildInputs
              [ contrib ]
            ]);
        };

      in
      {
        checks = {
          # Build the crate as part of `nix flake check` for convenience
          inherit contrib;

          # Run clippy (and deny all warnings) on the crate source,
          # again, resuing the dependency artifacts from above.
          #
          # Note that this is done as a separate derivation so that
          # we can block the CI if there are issues here, but not
          # prevent downstream consumers from building our crate by itself.
          contrib-clippy = craneLib.cargoClippy (contribCommon // {
            inherit cargoArtifacts;
            cargoClippyExtraArgs = "--all-targets -- --deny warnings";
          });

          contrib-fmt = craneLib.cargoFmt (contribCommon // {
            inherit src;
          });

          contrib-nextest = craneLib.cargoNextest (contribCommon // {
            inherit cargoArtifacts;
            partitions = 1;
            partitionType = "count";
          });
        };

        packages = {
          default = contrib;
          contrib = contrib;
          gst-depth-meta = gst-depth-meta;
          gst-colorizer = gst-colorizer;
        };

        devShells.default = craneLib.devShell (contribEnv // {
          checks = self.checks.${system};

          packages = [
            pkgs.gst_all_1.gstreamer
            pkgs.pkg-config
            pkgs.rust-analyzer
          ];
        });
      });
}


