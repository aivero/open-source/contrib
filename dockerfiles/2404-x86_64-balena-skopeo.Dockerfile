# docker build -t registry.gitlab.com/aivero/open-source/contrib/2404-x86_64-balena-skopeo/linux-x86_64 -f 2404-x86_64-balena-skopeo.Dockerfile .
FROM --platform=linux/amd64 gitlab.com:443/aivero/dependency_proxy/containers/amd64/ubuntu:24.04
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y --no-install-recommends zip zstd ca-certificates curl pigz wget unzip gnupg lsb-release jq  && install -m 0755 -d /etc/apt/keyrings && curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc && chmod a+r /etc/apt/keyrings/docker.asc && echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" |  tee /etc/apt/sources.list.d/docker.list > /dev/null && apt update && apt install -y --no-install-recommends docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin skopeo  && rm -rf /var/lib/apt/lists/*

ARG VERSION=v20.0.6
WORKDIR /balena
RUN wget https://github.com/balena-io/balena-cli/releases/download/v${VERSION}/balena-cli-v${VERSION}-linux-x64-standalone.zip -O balena.zip && unzip balena.zip && mv balena-cli/balena . && chmod +x balena && rm -rf balena-cli balena.zip
ENV PATH=$PATH:/balena
