# docker build -t registry.gitlab.com/aivero/open-source/contrib/noble-x86_64-balena/linux-x86_64 -f noble-x86_64-balena.Dockerfile .
FROM --platform=linux/amd64 gitlab.com:443/aivero/dependency_proxy/containers/amd64/ubuntu:noble
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install --no-install-recommends -y pigz wget unzip ca-certificates curl gnupg lsb-release jq skopeo gettext-base && \
    install -m 0755 -d /etc/apt/keyrings && \
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc && \
    chmod a+r /etc/apt/keyrings/docker.asc && \
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null && \
  apt-get update && apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin  && rm -rf /var/lib/apt/lists/*

ARG VERSION=20.0.6
WORKDIR /balena
RUN wget https://github.com/balena-io/balena-cli/releases/download/v${VERSION}/balena-cli-v${VERSION}-linux-x64-standalone.zip -O balena.zip && unzip balena.zip && mv balena-cli/balena . && chmod +x balena && rm -rf balena-cli balena.zip
ENV PATH=$PATH:/balena

# ruby-dev unifdef