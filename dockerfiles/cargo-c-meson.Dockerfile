# docker build -t registry.gitlab.com/aivero/prop/workspace/nonix-dcd-docker-base/linux-armv8-rpi3:latest . -f ./docker/deepcore-daemon-docker-base/nonix-linux-armv8-rpi3.Dockerfile

# docker run --rm -ti -v ~/.config/deepcore-daemon:/root/.config/deepcore-daemon -e GST_DEBUG=3,deepcore-daemon:7  -e UDEV=1 --privileged registry.gitlab.com/aivero/prop/workspace/nonix-dcd-docker-base/linux-armv8-rpi3:latest

FROM gitlab.com:443/aivero/dependency_proxy/containers/ubuntu:noble AS cargo-c-meson
# Prevent apt-get prompting for input
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get upgrade -y && apt-get install -y git build-essential cmake pkg-config curl software-properties-common libssl-dev wget --no-install-recommends && rm -rf /var/lib/apt/lists/*

# rust (change -Drs=disabled to enabled below)
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
ENV PATH="/root/.cargo/bin:${PATH}"
RUN cargo install cargo-c
RUN rustc --version

RUN apt-get update && apt-get upgrade -y && apt-get install -y flex bison bzip2 curl --no-install-recommends && add-apt-repository ppa:deadsnakes/ppa && apt-get update && apt-get install -y python3.11-dev python3-pip python3.11-venv --no-install-recommends && rm -rf /var/lib/apt/lists/*

ENV VIRTUAL_ENV=/opt/venv
RUN python3.11 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
RUN pip3 install meson tomli toml ninja setuptools

ARG NVCC_MAJOR_VERSION="12"
ARG NVCC_MINOR_VERSION="6"

ARG TARGETPLATFORM
RUN if [ "$TARGETPLATFORM" = "linux/arm64" ]; then wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2404/sbsa/cuda-keyring_1.1-1_all.deb; elif [ "$TARGETPLATFORM" = "linux/amd64" ]; then wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2404/x86_64/cuda-keyring_1.1-1_all.deb; else return 1; fi && dpkg -i cuda-keyring_1.1-1_all.deb && rm cuda-keyring_1.1-1_all.deb && apt-get update && apt-get install -y --no-install-recommends cuda-nvcc-${NVCC_MAJOR_VERSION}-${NVCC_MINOR_VERSION} && rm -rf /var/lib/apt/lists/*
ENV PATH=/usr/local/cuda-${NVCC_MAJOR_VERSION}.${NVCC_MINOR_VERSION}/bin:$PATH
ENV LD_LIBRARY_PATH=/usr/local/cuda-${NVCC_MAJOR_VERSION}.${NVCC_MINOR_VERSION}/lib64:$LD_LIBRARY_PATH
RUN nvcc --version
