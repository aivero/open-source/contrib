# docker build -t registry.gitlab.com/aivero/open-source/contrib/jammy-x86_64-docker/linux-x86_64 -f jammy-x86_64-docker.Dockerfile .
FROM --platform=linux/amd64 gitlab.com:443/aivero/dependency_proxy/containers/amd64/ubuntu:jammy
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install --no-install-recommends -y zstd pigz wget unzip ca-certificates curl gnupg lsb-release jq && install -m 0755 -d /etc/apt/keyrings && \
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc && chmod a+r /etc/apt/keyrings/docker.asc && \
  echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
    $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
    tee /etc/apt/sources.list.d/docker.list > /dev/null && \
  apt-get update && apt-get install --no-install-recommends -y  docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin skopeo