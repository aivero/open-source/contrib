# docker build -t registry.gitlab.com/aivero/open-source/contrib/alpine-balena-skopeo -f alpine-balena-skopeo.Dockerfile .
FROM --platform=linux/amd64 gitlab.com:443/aivero/dependency_proxy/containers/amd64/docker:26-dind

ARG BALENA_CLI_VERSION=v18.2.2
WORKDIR /balena
RUN wget https://github.com/balena-io/balena-cli/releases/download/${BALENA_CLI_VERSION}/balena-cli-${BALENA_CLI_VERSION}-linux-x64-standalone.zip -O balena.zip && unzip balena.zip && mv balena-cli/balena . && chmod +x balena && rm -rf balena-cli balena.zip
ENV PATH=$PATH:/balena
RUN apk add bash curl
