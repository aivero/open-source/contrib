# gst-colorizer

GStreamer plugin containing a colorizer, which converts 16-bit depth video into RGB.

# Getting started
```

## Build your own

If you have made changes to the `colorizer` that you wish to try, you may want to build the project locally:

> We assume you have gstreamer installed

```bash
sudo mkdir /opt/gst-colorizer
sudo chown -R $USER:$USER /opt/gst-colorizer

cd gst-colorizer
meson setup build --prefix=/opt/gst-colorizer --buildtype=release
ninja -C build install
export GST_PLUGIN_PATH=$GST_PLUGIN_PATH:$PWD/opt/gst-colorizer
```

Now you should see the plugin's element `colorizer`.

```bash
gst-inspect-1.0 colorizer
```

An example of a pipeline:

```bash
# Please replace XXXXXXXX with the serial on your RealSense camera
export RS_SERIAL=XXXXXXXX
gst-launch-1.0 \
realsensesrc serial=$RS_SERIAL enable-depth=true ! \
rgbddemux name=realsense_demux \
realsense_demux.src_depth ! queue ! colorizer near-cut=300 far-cut=3000 ! glimagesink 
```