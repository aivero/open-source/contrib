#!/bin/sh -e

here=$(dirname "$(realpath "$0")")

mkdir -p "$HOME/.config/nix"
{
    echo 'experimental-features = nix-command flakes'
    echo 'max-jobs = auto'
} >"$HOME/.config/nix/nix.conf"
